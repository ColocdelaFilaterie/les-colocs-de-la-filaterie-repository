<!-- header start -->
<!-- classes:  -->
<!-- "fixed": enables fixed navigation mode (sticky menu) e.g. class="header fixed clearfix" -->
<!-- "fixed-desktop": enables fixed navigation only for desktop devices e.g. class="header fixed fixed-desktop clearfix" -->
<!-- "fixed-all": enables fixed navigation only for all devices desktop and mobile e.g. class="header fixed fixed-desktop clearfix" -->
<!-- "dark": dark version of header e.g. class="header dark clearfix" -->
<!-- "centered": mandatory class for the centered logo layout -->
<!-- ================ -->
<header class="header fixed">
 <div class="container">
    <div class="row">
      <div class="col-md-auto hidden-md-down pl-3">
        <!-- header-first start -->
        <!-- ================ -->
        <div class="header-first clearfix">
          <!-- logo -->
          <div id="logo" class="logo">
            <a href="index.php"><img id="logo_img" src="images/logo-colocs3.png" alt="Les Colocs"></a>
          </div>
          <!-- name-and-slogan -->
          <div class="site-slogan">
           <center>Les Colocs de la Filaterie</center>
          </div>
        </div>
        <!-- header-first end -->
      </div>
      <div class="col-lg-8 ml-lg-auto">
        <!-- header-second start -->
        <!-- ================ -->
        <div class="header-second clearfix">
          <!-- main-navigation start -->
          <!-- classes: -->
          <!-- "onclick": Makes the dropdowns open on click, this the default bootstrap behavior e.g. class="main-navigation onclick" -->
          <!-- "animated": Enables animations on dropdowns opening e.g. class="main-navigation animated" -->
          <!-- ================ -->
          <div class="main-navigation main-navigation--mega-menu  animated">
            <nav class="navbar navbar-expand-lg navbar-light p-0">
              <div class="navbar-brand clearfix hidden-lg-up">
                <!-- logo -->
                <div id="logo-mobile" class="logo">
                  <a href="index.php"><img id="logo-img-mobile" src="images/logo-colocs3.png" alt="Les Colocs"></a>
                </div>
                <!-- name-and-slogan -->
                <div class="site-slogan">
                  <center>Les Colocs de la Filaterie</center>
                </div>
              </div>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse-1" aria-controls="navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon">
                </span>
              </button>
              <div class="collapse navbar-collapse" id="navbar-collapse-1">
                <!-- main-menu -->
                <ul class="navbar-nav ml-xl-auto">
                  <!-- mega-menu start -->
                  <li class="nav-item dropdown active mega-menu mega-menu--wide">
                    <a href="index.php" class="nav-link dropdown-toggle" aria-haspopup="true" aria-expanded="false">Accueil</a>
                  </li>
                  <!-- mega-menu end -->
                  <!-- mega-menu start -->
                  <li class="nav-item dropdown  mega-menu mega-menu--wide">
                    <a href="colocations.php" class="nav-link dropdown-toggle" aria-haspopup="true" aria-expanded="false">Colocations</a>
                  </li>
                  <!-- mega-menu end -->
                  <li class="nav-item dropdown ">
                    <a href="studios.php" class="nav-link dropdown-toggle" aria-haspopup="true" aria-expanded="false">Studios</a>
                  </li>
                  <!-- mega-menu start -->
                  <li class="nav-item dropdown  mega-menu mega-menu--narrow">
                    <a href="page-faq.php" class="nav-link dropdown-toggle" aria-haspopup="true" aria-expanded="false">FAQ</a>
                  </li>
                </ul>
                <!-- main-menu end -->
              </div>
            </nav>
          </div>
          <!-- main-navigation end -->
        </div>
        <!-- header-second end -->
      </div>
      <div class="col-auto hidden-md-down pl-0 pl-md-1">
        <!-- header dropdown buttons -->
        <div class="header-dropdown-buttons">
          <a href="page-contact.php" class="btn btn-sm btn-default">Nous contacter<i class="fa fa-envelope-o pl-1"></i>
          </a>
        </div>
        <!-- header dropdown buttons end-->
      </div>
    </div>
 </div>
</header>
<!-- header end -->