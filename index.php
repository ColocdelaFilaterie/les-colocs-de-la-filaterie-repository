<!DOCTYPE html>
<!--[if IE 9]> <html lang="zxx" class="ie9"> <![endif]-->
<!--[if gt IE 9]> <html lang="zxx" class="ie"> <![endif]-->
<!--[if !IE]><!-->
<html dir="ltr" lang="fr">
  <!--<![endif]-->

  <head>
    <meta charset="utf-8">
    <title>Les Colocs de la Filaterie</title>
    <meta name="description" content="Locations et colocations d'appartements meublés à Rumilly 74 Haute-Savoie, Annecy">
    <meta name="author" content="htmlcoder.me">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.ico">

    <!-- Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Font Awesome CSS -->
    <link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Fontello CSS -->
    <link href="fonts/fontello/css/fontello.css" rel="stylesheet">

    <!-- Plugins -->
    <link href="plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="plugins/rs-plugin-5/css/settings.css" rel="stylesheet">
    <link href="plugins/rs-plugin-5/css/layers.css" rel="stylesheet">
    <link href="plugins/rs-plugin-5/css/navigation.css" rel="stylesheet">
    <link href="css/animations.css" rel="stylesheet">
    <link href="plugins/owlcarousel2/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="plugins/owlcarousel2/assets/owl.theme.default.min.css" rel="stylesheet">
    <link href="plugins/hover/hover-min.css" rel="stylesheet">    
    
    <!-- The Project's core CSS file -->
    <!-- Use css/rtl_style.css for RTL version -->
    <link href="css/style.css" rel="stylesheet" >
    <!-- The Project's Typography CSS file, includes used fonts -->
    <!-- Used font for body: Roboto -->
    <!-- Used font for headings: Raleway -->
    <!-- Use css/rtl_typography-default.css for RTL version -->
    <link href="css/typography-default.css" rel="stylesheet" >
    <!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
    <link href="css/skins/gold.css" rel="stylesheet">
    

    <!-- Custom css --> 
    <link href="css/custom.css" rel="stylesheet">
  </head>

 <!-- body classes:  -->
  <!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
  <!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
  <!-- "transparent-header": makes the header transparent and pulls the banner to top -->
  <!-- "gradient-background-header": applies gradient background to header -->
  <!-- "page-loader-1 ... page-loader-6": add a page loader to the page (more info @components-page-loaders.html) -->
  <body class="">

    <!-- scrollToTop -->
    <!-- ================ -->
    <div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>

    <!-- page wrapper start -->
    <!-- ================ -->
    <div class="page-wrapper">

      <?php include("menu.php");  ?>
        
      <!-- header-container end -->
      <!-- banner start -->
      <!-- ================ -->
      <div class="banner dark-translucent-bg padding-bottom-clear" style="background-image:url('images/appartement-meuble-rumilly.jpg');background-position: 50% 70%;">
        <div class="container">
          <div class="row justify-content-lg-center">
            <div class="col-lg-8 text-center pv-20">
              <h2 class="title" data-animation-effect="fadeIn" data-effect-delay="100"> <strong>Les Colocs</strong></h2>
              <div class="separator mt-10" data-animation-effect="fadeIn" data-effect-delay="100"></div>
              <p class="text-center" data-animation-effect="fadeIn" data-effect-delay="100">Votre appartement meublé à Rumilly en 2 clics !</p>
            </div>
          </div>
        </div>
      </div>
      <!-- banner end -->


      <!-- section start -->
      <!-- ================ -->
      <section class="light-gray-bg pv-40 border-clear">
        <div class="container">
          <div class="row justify-content-lg-center">
            <div class="col-lg-8">
              <h2 class="text-center title"><strong> les appartements</strong></h2>
              <div class="separator"></div>
              <p class="large text-center">Découvrez tous les appartements ci dessous:</p>
            </div>
          </div>
        </div>
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-4 masonry-grid-item">
                      <div class="listing-item white-bg bordered mb-20">
                        <div class="overlay-container">
                          <img src="images/studio-0.jpg" alt="">
                          <span class="badge">Disponible Juin 2020</span>
                          <a class="overlay-link" href="le-studio.php"><i class="fa fa-plus"></i></a>
                        </div>
                        <div class="body">
                          <h3><a href="le-studio.php">Le Studio</a></h3>
                          <p class="small">Petit et très cosy, vous serez ravis par ce studio moderne, simple et lumineux.</p>
                          <div class="elements-list clearfix">
                           <span class="price">Appartement complet: 450 € /mois </span>
                         </br>
                            <a href="le-studio.php" class="pull-right margin-clear btn btn-sm btn-default-transparent">Voir l'offre</a>
                          </div>
                        </div>
                      </div>
            </div>

            <div class="col-lg-4 masonry-grid-item">
                      <div class="listing-item white-bg bordered mb-20">
                        <div class="overlay-container">
                          <img src="images/62-0.jpg" alt="">
                          <span class="badge">Disponible Mars 2020</span>
                          <a class="overlay-link" href="le-soixante-deux.php"><i class="fa fa-plus"></i></a>
                        </div>
                        <div class="body">
                          <h3><a href="le-soixante-deux.php">Le Soixante-Deux</a></h3>
                          <p class="small">Authentique et pratique, ce grand appartement vous conviendra parfaitement. </p>
                          <div class="elements-list clearfix">
                           <span class="price">Chambre simple: 380 € /mois</span><span class="price">Chambre double: 390 € /mois </span>
                            <a href="le-soixante-deux.php" class="pull-right margin-clear btn btn-sm btn-default-transparent">Voir l'offre</a>
                          </div>
                        </div>
                      </div>
            </div>

            <div class="col-lg-4 masonry-grid-item">
                      <div class="listing-item white-bg bordered mb-20">
                        <div class="overlay-container">
                          <img src="images/fenetres-0.jpg" alt="">
                          <span class="badge">Disponible Janvier 2020</span>
                          <a class="overlay-link" href="mille-fenetres.php"><i class="fa fa-plus"></i></a>
                        </div>
                        <div class="body">
                          <h3><a href="mille-fenetres.php">Mille Fenêtres</a></h3>
                          <p class="small">Extrêmement lumineux, cet appartement sera respectueux de votre intimité.</p>
                          <div class="elements-list clearfix">
                          <span class="price">Chambre seule: 360 € /mois</span><span class="price">Chambre double: 390 € /mois  </span>
                            <a href="mille-fenetres.php" class="pull-right margin-clear btn btn-sm btn-default-transparent">Voir l'offre</a>
                          </div>
                        </div>
                      </div>
            </div>

            <div class="col-lg-4 masonry-grid-item">
                      <div class="listing-item white-bg bordered mb-20">
                        <div class="overlay-container">
                          <img src="images/cabanes-0.jpg" alt="">
                          <span class="badge">Disponible Août 2020</span>
                          <a class="overlay-link" href="rose-bonbon.php"><i class="fa fa-plus"></i></a>
                        </div>
                        <div class="body">
                          <h3><a href="rose-bonbon.php">Rose Bonbon</a></h3>
                          <p class="small">Avec son balcon, vous découvrirez un appartement très ouvert.</p>
                          <div class="elements-list clearfix">
                            <span class="price">Appartement complet: 540 € /mois</span>
                          </br>
                            <a href="rose-bonbon.php" class="pull-right margin-clear btn btn-sm btn-default-transparent">Voir l'offre</a>
                          </div>
                        </div>
                      </div>
            </div>

            <div class="col-lg-4 masonry-grid-item">
                      <div class="listing-item white-bg bordered mb-20">
                        <div class="overlay-container">
                          <img src="images/cabanes-0.jpg" alt="">
                          <span class="badge">Disponible Juin 2020</span>
                          <a class="overlay-link" href="mille-cabanes.php"><i class="fa fa-plus"></i></a>
                        </div>
                        <div class="body">
                          <h3><a href="mille-cabanes.php">Mille Cabanes</a></h3>
                          <p class="small">Un tout petit prix pour cet appartement super joli !</p>
                          <div class="elements-list clearfix">
                            <span class="price">Chambre seule: 350 € /mois</span><span class="price">Chambre double: 380 € /mois</span>
                            <a href="mille-cabanes.php" class="pull-right margin-clear btn btn-sm btn-default-transparent">Voir l'offre</a>
                          </div>
                        </div>
                      </div>
            </div>

          </div>
        </div>
      </section>
      <!-- section end -->

      <!-- section start -->
      <!-- ================ -->
      <section class="dark-translucent-bg background-img-9 pv-40" style="background-position: 50% 50%;">
        <div class="container">
          <div class="row">
            <div class="col-lg-4">
              <div class="testimonial text-center">
                <h3>Juste parfait!</h3>
                <div class="separator"></div>
                <div class="testimonial-body">
                  <blockquote>
                    <p>Idéal pour un premier appartement, j'ai adoré son côté très cocooning.</p>
                  </blockquote>
                  <div class="testimonial-info-1">- Virginie</div>
                  <div class="testimonial-info-2">Le Studio</div>
                </div>
              </div>
            </div>
           <div class="col-lg-4">
              <div class="testimonial text-center">
                <h3>Tout à vélo!</h3>
                <div class="separator"></div>
                <div class="testimonial-body">
                  <blockquote>
                    <p>Très bien situé, en plein centre-ville, tous les commerces sont à proximités.</p>
                  </blockquote>
                  <div class="testimonial-info-1">- Laëtitia</div>
                  <div class="testimonial-info-2">Le Soixante-Deux</div>
                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="testimonial text-center">
                <h3>Vive la colocation!</h3>
                <div class="separator"></div>
                <div class="testimonial-body">
                  <blockquote>
                    <p>La colocation m'a permis de découvrir Rumilly le temps de mes études.</p>
                  </blockquote>
                  <div class="testimonial-info-1">- Carla</div>
                  <div class="testimonial-info-2">Mille Fenêtres</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- section end -->

      <!-- section -->
      <!-- ================ -->
      <section class="light-gray-bg pv-40">
        <div class="container">
          <div class="row justify-content-lg-center">
            <div class="col-lg-8">
              <h2 class="text-center">Photos de <strong>Rumilly</strong></h2>
              <div class="separator"></div>
              <p class="large text-center">Centre historique et alentours.</p>
              <br>
            </div>
          </div>
          <div class="row grid-space-2">
            <div class="col-lg-6">
              <div class="image-box text-center">
                <div class="overlay-container">
                  <img src="images/La-Filaterie.jpg" alt="La Filaterie 1 Rue Filaterie Rumilly Haute Savoie">
                  <div class="overlay-top">
                    <div class="text">
                      <h3>La Filaterie</h3>
                      <p class="small">L'immeuble des logements: 1 Rue Filaterie 74150 Rumilly.</p>
                    </div>
                  </div>
                  <div class="overlay-bottom">
                    <div class="links">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="image-box text-center">
                <div class="overlay-container">
                  <img src="images/plan-d-eau.jpg" alt="plan d'eau Rumilly Haute Savoie">
                  <div class="overlay-top">
                    <div class="text">
                      <h3>Plan d'eau</h3>
                      <p class="small">Base de loisirs et de baignade de Rumilly</p>
                    </div>
                  </div>
                  <div class="overlay-bottom">
                    <div class="links">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="image-box text-center">
                <div class="overlay-container">
                  <img src="images/semnozcopie.jpg" alt="Station du Semnoz Rumilly Haute Savoie">
                  <div class="overlay-top">
                    <div class="text">
                      <h3>Station du Semnoz</h3>
                      <p class="small">Station de ski à 20 minutes de Rumilly</p>
                    </div>
                  </div>
                  <div class="overlay-bottom">
                    <div class="links">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="image-box text-center">
                <div class="overlay-container">
                  <img src="images/marche-grenettecopie.jpg" alt="place Grenette Rumilly Haute Savoie">
                  <div class="overlay-top">
                    <div class="text">
                      <h3>Place Grenette</h3>
                      <p class="small">Le marché du jeudi et du samedi matin.</p>
                    </div>
                  </div>
                  <div class="overlay-bottom">
                    <div class="links">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- section end -->

      <!-- footer top start -->
      <!-- ================ -->
      <div class="dark-bg footer-top animated-text default-hovered">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="call-to-action text-center">
                <div class="row">
                  <div class="col-md-8">
                    <h1 class="title">Pour ne rien louper</h1>
                    <p class="text-white">Les réponses à vos questions sont par ici!</p>
                  </div>
                  <div class="col-md-4">
                    <br>
                    <p><a href="page-faq.php" class="btn btn-lg btn-default btn-animated">Voir<i class="fa fa-arrow-right pl-20"></i></a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- footer top end -->


      <!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
      <!-- ================ -->
      <footer id="footer" class="clearfix">
        <?php include("bas-de-page.php");  ?>
        <?php include("footer.php");  ?>
      </footer>
      <!-- footer end -->
     </div>
    <!-- page-wrapper end -->

    <!-- JavaScript files placed at the end of the document so the pages load faster -->
    <!-- ================================================== -->
    <!-- Jquery and Bootstap core js files -->
    <script type="text/javascript" src="plugins/jquery.min.js"></script>
    <script type="text/javascript" src="plugins/popper.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Modernizr javascript -->
    <script type="text/javascript" src="plugins/modernizr.js"></script>
    <!-- jQuery Revolution Slider  -->
    <script type="text/javascript" src="plugins/rs-plugin-5/js/jquery.themepunch.tools.min.js?rev=5.0"></script>
    <script type="text/javascript" src="plugins/rs-plugin-5/js/jquery.themepunch.revolution.min.js?rev=5.0"></script>
    <!-- Isotope javascript -->
    <script type="text/javascript" src="plugins/isotope/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="plugins/isotope/isotope.pkgd.min.js"></script>
    <!-- Magnific Popup javascript -->
    <script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Appear javascript -->
    <script type="text/javascript" src="plugins/waypoints/jquery.waypoints.min.js"></script>
    <script type="text/javascript" src="plugins/waypoints/sticky.min.js"></script>
    <!-- Count To javascript -->
    <script type="text/javascript" src="plugins/jquery.countTo.js"></script>
    <!-- Parallax javascript -->
    <script src="plugins/jquery.parallax-1.1.3.js"></script>
    <!-- Contact form -->
    <script src="plugins/jquery.validate.js"></script>
    <!-- Google Maps javascript--> 
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;key=your_google_map_key"></script>
    <script type="text/javascript" src="js/google.map.config.js"></script>
    <!-- Google Maps javascript -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASvJ4qWz-yTDlzrTyKUOWhMTzge1RKh70"></script>
    <script src="js/google.map.config.js"></script>
    <!-- Owl carousel javascript -->
    <script type="text/javascript" src="plugins/owlcarousel2/owl.carousel.min.js"></script>
    <!-- Pace javascript -->
    <script type="text/javascript" src="plugins/pace/pace.min.js"></script>
    <!-- Initialization of Plugins -->
    <script type="text/javascript" src="js/template.js"></script>
    <!-- Custom Scripts -->
    <script type="text/javascript" src="js/custom.js"></script>
  </body>
</html>
