<div class="alert alert-success hidden-xs-up" id="MessageSent2">
      Votre message a bien été envoyé, nous vous répondrons dès que possible.
</div>
  
  <div class="alert alert-danger hidden-xs-up" id="MessageNotSent2">
      Oops! Il doit manquer une information... Réessayez plus tard.
  </div>
  <form id="footer-form" class="margin-clear">
    <div class="form-group has-feedback">
        <label class="sr-only" for="name2">Nom</label>
        <input type="text" class="form-control" id="name2" placeholder="Nom" name="name2">
        <i class="fa fa-user form-control-feedback"></i>
    </div>

      <div class="form-group has-feedback">
        <label class="sr-only" for="email2">Adresse Email</label>
        <input type="email" class="form-control" id="email2" placeholder="Adresse Email" name="email2">
        <i class="fa fa-envelope form-control-feedback"></i>
      </div>

        <div class="form-group has-feedback">
          <label class="sr-only" for="message2">Message</label>
          <textarea class="form-control" rows="6" id="message2" placeholder="Message" name="message2"></textarea>
          <i class="fa fa-pencil form-control-feedback"></i>
        </div>
        
      <input type="submit" value="Envoyer" class="margin-clear submit-button btn btn-default">
  </form>