<!DOCTYPE html>
<!--[if IE 9]> <html lang="zxx" class="ie9"> <![endif]-->
<!--[if gt IE 9]> <html lang="zxx" class="ie"> <![endif]-->
<!--[if !IE]><!-->
<html dir="ltr" lang="fran">
	<!--<![endif]-->

  <head>
    <meta charset="utf-8">
    <title>Mille Cabanes/Chambre double</title>
    <meta name="description" content="Chambre double appartement Mille Cabanes la Filaterie, Rumilly 74 Haute Savoie">
    <meta name="author" content="htmlcoder.me">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.ico">

    <!-- Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Font Awesome CSS -->
    <link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Fontello CSS -->
    <link href="fonts/fontello/css/fontello.css" rel="stylesheet">

    <!-- Plugins -->
    <link href="plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="css/animations.css" rel="stylesheet">
    <link href="plugins/owlcarousel2/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="plugins/owlcarousel2/assets/owl.theme.default.min.css" rel="stylesheet">
    <link href="plugins/hover/hover-min.css" rel="stylesheet">		
    
    <!-- The Project's core CSS file -->
    <!-- Use css/rtl_style.css for RTL version -->
    <link href="css/style.css" rel="stylesheet" >
    <!-- The Project's Typography CSS file, includes used fonts -->
    <!-- Used font for body: Roboto -->
    <!-- Used font for headings: Raleway -->
    <!-- Use css/rtl_typography-default.css for RTL version -->
    <link href="css/typography-default.css" rel="stylesheet" >
    <!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
    <link href="css/skins/gold.css" rel="stylesheet">
    

    <!-- Custom css --> 
    <link href="css/custom.css" rel="stylesheet">  
  </head>

  <!-- body classes:  -->
  <!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
  <!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
  <!-- "transparent-header": makes the header transparent and pulls the banner to top -->
  <!-- "gradient-background-header": applies gradient background to header -->
  <!-- "page-loader-1 ... page-loader-6": add a page loader to the page (more info @components-page-loaders.html) -->
  <body class=" ">

    <!-- scrollToTop -->
    <!-- ================ -->
    <div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>

    <!-- page wrapper start -->
    <!-- ================ -->
    <div class="page-wrapper">
      <!-- header-container start -->
      <div class="header-container">

        <?php include("menu.php");  ?>
      </div>
      <!-- header-container end -->

      <!-- main-container start -->
      <!-- ================ -->
      <section class="main-container">

        <div class="container">
          <div class="row">

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-12">

              <!-- page-title start -->
              <!-- ================ -->
              <h1 class="page-title text-center">Chambre Double</h1>
              <p class="large text-center">Cette chambre fait partie de Mille Cabanes<br>Appartement en colocation</p>
              <div class="separator"></div>
              <!-- page-title end -->

              <div class="row">
                <div class="col-lg-4 col-xl-5">
                  <!-- pills start -->
                  <!-- ================ -->
      
                  <!-- Tab panes -->
                  <div class="tab-content clear-style">
                    <div class="tab-pane active" id="pill-1">
                      <div class="owl-carousel content-slider-with-thumbs mb-20">
                        <div class="overlay-container overlay-visible">
                          <img src="images/cabanes-double-coupee.jpg" alt="">
                          <a href="images/cabanes-double.jpg" class="owl-carousel--popup-img overlay-link" title="chambre double vision complète"><i class="fa fa-search-plus"></i></a>
                        </div>
                        <div class="overlay-container overlay-visible">
                          <img src="images/cabanes-double-2-coupee.jpg" alt="">
                          <a href="images/cabanes-double-2.jpg" class="owl-carousel--popup-img overlay-link" title="lit de la chambre double"><i class="fa fa-search-plus"></i></a>
                        </div>
                        <div class="overlay-container overlay-visible">
                          <img src="images/cuisine-cabanes-coupee.jpg" alt="">
                          <a href="images/cuisine-cabanes.jpg" class="owl-carousel--popup-img overlay-link" title="cuisine"><i class="fa fa-search-plus"></i></a>
                        </div>
                        <div class="overlay-container overlay-visible">
                          <img src="images/salle-bains-cabanes-coupee.jpg" alt="">
                          <a href="images/salle-bains-cabanes.jpg" class="owl-carousel--popup-img overlay-link" title="salle de bains"><i class="fa fa-search-plus"></i></a>
                        </div>
                      </div>

                      <div class="content-slider-thumbs-container">
                        <div class="owl-carousel content-slider-thumbs">
                          <div class="owl-nav-thumb">
                            <img src="images/cabanes-double-coupee.jpg" alt="">
                          </div>
                          <div class="owl-nav-thumb">
                            <img src="images/cabanes-double-2-coupee.jpg" alt="">
                          </div>
                          <div class="owl-nav-thumb">
                            <img src="images/cuisine-cabanes-coupee.jpg" alt="">
                          </div>
                          <div class="owl-nav-thumb">
                            <img src="images/salle-bains-cabanes-coupee.jpg" alt="">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- pills end -->
                  <hr class="mb-10">
                </div>
                <div class="col-lg-8 col-xl-7 pv-30">
                  <h2 class="mt-4">Description</h2>
                  <p>Belle exposition pour cet appartement de 47 m² au centre ville de Rumilly, proche de toutes commodités. Il est composé de deux chambres, une salle de bains avec baignoire, une cuisine équipée et un salon. Chauffage au sol électrique.</p>
                  <h4>Déco sympa, style scandinave, ambiance cocooning</h4>
                  <p>Chambre équipée d'un lit double, louée avec une couette, un matelas, des cintres, un miroir et un porte manteaux.</p>
                  <h4 class="space-top">Spécifications</h4>
                  <hr>
                  <dl class="row">
                    <dt class="col-sm-3 text-sm-right">Surface</dt>
                    <dd class="col-sm-9">47 m²</dd>
                    <dt class="col-sm-3 text-sm-right">Chambre 2</dt>
                    <dd class="col-sm-9">Chambre équipée d'un lit double, d'un grand dressing.</dd>
                    <dt class="col-sm-3 text-sm-right">Equipements</dt>
                    <dd class="col-sm-9">L'appartement est équipé d'une salle de bains avec baignoire, d'une cuisine avec frigo, four, hotte, et lave-vaisselle ainsi qu'une télévision et une machine à laver. Il est doté d'un chauffage au sol.</dd>
                    <dt class="col-sm-3 text-sm-right">Commodité</dt>
                    <dd class="col-sm-9">Ascenseur, cave individuelle, proche de petits commerces comme la boulangerie ou un marché hebdommadaire.</dd>
                    <dt class="col-sm-3 text-sm-right">Transports</dt>
                    <dd class="col-sm-9">Facilité de stationnement au pied de la résidence, parking public à 3min à pied, transport en commun: bus.</dd>
                    <dt class="col-sm-3 text-sm-right">Adresse</dt>
                    <dd class="col-sm-9"><a href="https://goo.gl/maps/zhJ8Be8MuYTDDRkk6" target="_blank">1 Rue Filaterie 74150 Rumilly 4e étage</a></dd>
                  </dl>
                  <hr>
                  <span class="product price"><i class="icon-tag pr-10"></i>380 €/mois</span>
                  <div class="product elements-list pull-right clearfix">
                </div>
              </div>
            </div>
            <!-- main end -->
            <!-- comments form start -->

          <div class="row">
            <div class="col-lg-8 col-xl-8 pv-30">
              <div class="comments-form">
                <h2 class="title">Réserver</h2>
                <?php include("formulaire.php");  ?>
              </div>
            </div>
              <!-- comments form end -->

             <!-- sidebar start -->
            <!-- ================ -->
            <aside class="col-lg-4 col-xl-4 ml-xl-auto">
              <div class="sidebar">
                <div class="block clearfix">
                  <h3 class="title">Autres Chambres</h3>
                  <div class="separator-2"></div>
                  <div class="media margin-clear">
                    <div class="d-flex pr-2">
                      <div class="overlay-container">
                        <img class="media-object" src="images/fenetres-simple-coupee.jpg" alt="lit de la chambre simple de Mille Fenêtres">
                        <a href="chambre-simple-fenetres.php" class="overlay-link small"><i class="icon-plus-1"></i></a>
                      </div>
                    </div>
                    <div class="media-body">
                      <h6 class="media-heading"><a href="chambre-simple-fenetres.php">Chambre simple</br>Mille Fenêtres</a></h6>
                      <p class="price">360 €/mois</p>
                    </div>
                  </div>
                  <hr>
                  <div class="media margin-clear">
                    <div class="d-flex pr-2">
                      <div class="overlay-container">
                        <img class="media-object" src="images/fenetres-doubles-coupee.jpg" alt="lit de la chambre double de Mille Fenêtres">
                        <a href="chambre-double-fenetres.php" class="overlay-link small"><i class="icon-plus-1"></i></a>
                      </div>
                    </div>
                    <div class="media-body">
                      <h6 class="media-heading"><a href="chambre-double-fenetres.php">Chambre double</br>Mille Fenêtres</a></h6>
                      <p class="price">390 €/mois</p>
                    </div>
                  </div>
                  <hr>
                  <div class="media margin-clear">
                    <div class="d-flex pr-2">
                      <div class="overlay-container">
                        <img class="media-object" src="images/chambre-simple-62.jpg" alt="lit de la chambre simple du Soixante-Deux">
                        <a href="chambre-simple-62.php" class="overlay-link small"><i class="icon-plus-1"></i></a>
                      </div>
                    </div>
                    <div class="media-body">
                      <h6 class="media-heading"><a href="chambre-simple-62.php">Chambre simple</br>Le Soixante-deux</a></h6>
                      <p class="price">380 €/mois</p>
                    </div>
                  </div>
                  <hr>
                  <div class="media margin-clear">
                    <div class="d-flex pr-2">
                      <div class="overlay-container">
                        <img class="media-object" src="images/chambre-double-62-coupee.jpg" alt="lit de la chambre double du Soixante-Deux">
                        <a href="chambre-double-62.php" class="overlay-link small"><i class="icon-plus-1"></i></a>
                      </div>
                    </div>
                    <div class="media-body">
                      <h6 class="media-heading"><a href="chambre-double-62.php">Chambre double</br>Le Soixante-deux</a></h6>
                      <p class="price">390 €/mois</p>
                    </div>
                  </div>
                </div>
              </div>
            </aside>
            <!-- sidebar end -->
          </div>  

          </div>
        </div>
      </section>
      <!-- main-container end -->

        <?php include("footer.php");  ?>

    </div>
    <!-- page-wrapper end -->

    <!-- JavaScript files placed at the end of the document so the pages load faster -->
    <!-- ================================================== -->
    <!-- Jquery and Bootstap core js files -->
    <script type="text/javascript" src="plugins/jquery.min.js"></script>
    <script type="text/javascript" src="plugins/popper.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Modernizr javascript -->
    <script type="text/javascript" src="plugins/modernizr.js"></script>
    <!-- Isotope javascript -->
    <script type="text/javascript" src="plugins/isotope/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="plugins/isotope/isotope.pkgd.min.js"></script>
    <!-- Magnific Popup javascript -->
    <script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Appear javascript -->
    <script type="text/javascript" src="plugins/waypoints/jquery.waypoints.min.js"></script>
    <script type="text/javascript" src="plugins/waypoints/sticky.min.js"></script>
    <!-- Count To javascript -->
    <script type="text/javascript" src="plugins/jquery.countTo.js"></script>
    <!-- Parallax javascript -->
    <script src="plugins/jquery.parallax-1.1.3.js"></script>
    <!-- Contact form -->
    <script src="plugins/jquery.validate.js"></script>
    <!-- Owl carousel javascript -->
    <script type="text/javascript" src="plugins/owlcarousel2/owl.carousel.min.js"></script>
    <!-- Initialization of Plugins -->
    <script type="text/javascript" src="js/template.js"></script>
    <!-- Custom Scripts -->
    <script type="text/javascript" src="js/custom.js"></script>

  </body>
</html>
