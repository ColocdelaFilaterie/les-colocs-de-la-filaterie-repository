<!-- .footer start -->
        <!-- ================ -->
        <div class="footer">
          <div class="container">
            <div class="footer-inner">
              <div class="row">
                <div class="col-lg-8">
                  <div class="footer-content">
                    <div class="logo-footer">
                      <img id="logo-footer" src="images/favicon.ico" alt="">
                    </div>
                    <div class="row">
                      <div class="col-lg-6">
                        <p>"Les Colocs de la Filaterie" vous propose des appartements meublés à la location, seul ou en colocation, en plein centre de Rumilly.</br>Une formule simple et rapide pour vous loger.</p>
                        <ul class="social-links circle animated-effect-1">
                          <li class="facebook"><a target="_blank" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a></li>
                          <li class="pinterest"><a target="_blank" href="http://www.pinterest.com"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                        <ul class="list-icons">
                          <li><i class="fa fa-map-marker pr-10 text-default"></i>1 rue de la Filaterie 74150 RUMILLY</li>
                          <li><i class="fa fa-phone pr-10 text-default"></i>06 26 98 53 76</li>
                        </ul>
                      </div>
                      <div class="col-lg-6">
                          <div id="map-canvas">
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="footer-content">
                    <h2 class="title">Nous contacter</h2>
                      <br>
                    <div class="alert alert-success hidden-xs-up" id="MessageSent2">
                      Votre message a atteri dans notre boite mail ! Nous vous répondrons dès que possible.
                    </div>
                    <div class="alert alert-danger hidden-xs-up" id="MessageNotSent2">
                      Oops! Il doit manquer une information... Réessayez plus tard.
                    </div>
                    <form id="footer-form" class="margin-clear">
                      <div class="form-group has-feedback">
                        <label class="sr-only" for="name2">Nom</label>
                        <input type="text" class="form-control" id="name2" placeholder="NGm" name="name2">
                        <i class="fa fa-user form-control-feedback"></i>
                      </div>
                      <div class="form-group has-feedback">
                        <label class="sr-only" for="email2">Adresse Email</label>
                        <input type="email" class="form-control" id="email2" placeholder="Adresse Email" name="email2">
                        <i class="fa fa-envelope form-control-feedback"></i>
                      </div>
                      <div class="form-group has-feedback">
                        <label class="sr-only" for="message2">Message</label>
                        <textarea class="form-control" rows="6" id="message2" placeholder="Message" name="message2"></textarea>
                        <i class="fa fa-pencil form-control-feedback"></i>
                      </div>
                      <input type="submit" value="Envoyer" class="margin-clear submit-button btn btn-default">
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- .footer end -->