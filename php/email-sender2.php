<?php
session_cache_limiter('nocache');
header('Expires: ' . gmdate('r', 0));
header('Content-type: application/json');

$Recipient = 'balmer.yannick@gmail.com'; // <-- Set your email here

if($Recipient) {

	$Date1 = filter_var($_POST['date1'], FILTER_SANITIZE_STRING);
	$Date2 = filter_var($_POST['date2'], FILTER_SANITIZE_STRING);
	$Email = filter_var($_POST['email-date'], FILTER_SANITIZE_EMAIL);
//	$Appartement = filter_var($_POST['appartement'], FILTER_SANITIZE_STRING);
	if (isset($_POST['guests'])) {
		$Guests = filter_var($_POST['guests'], FILTER_SANITIZE_STRING);
	} else {
		$Guests = "";
	}
	if (isset($_POST['events'])) {
		$Events = filter_var($_POST['events'], FILTER_SANITIZE_STRING);
	} else {
		$Events = "";
	}
	if (isset($_POST['category'])) {
		$Category = filter_var($_POST['category'], FILTER_SANITIZE_STRING);
	} else {
		$Category = "";
	}

	$Email_body = "";
	$Email_body .= "De: " . $Date1 . "\n" .
				   "A: " . $Date2 . "\n" .
				   "Email: " . $Email . "\n" .
				   //"Appartement: " . $Appartement . "\n" .

	$Email_headers = "";
	$Email_headers .= 'From: ' . $Email . ' <' . $Email . '>' . "\r\n".
					  "Reply-To: " .  $Email . "\r\n";

	$sent = mail($Recipient, $Subject, $Email_body, $Email_headers);

if ($sent){
		$emailResult = array ('sent'=>'yes');
	} else{
		$emailResult = array ('sent'=>'no');
	}

	echo json_encode($emailResult);

} else {

	$emailResult = array ('sent'=>'no');
	echo json_encode($emailResult);

}
?>