<!-- .footer start -->
<!-- ================ -->
<div class="footer">
  <div class="container">
    <div class="footer-inner">
      <div class="row">
        <div class="col-lg-6">
          <div class="footer-content">
            <div class="logo-footer">
              <img id="logo-footer" src="images/favicon.ico" alt="">
            </div>
            <div class="row">
              <div class="col-lg-6">
                <p>"Les Colocs de la Filaterie" vous propose des appartements meublés à la location, seul ou en colocation, en plein centre de Rumilly.</br>Une formule simple et rapide pour vous loger.</p>
                <ul class="social-links circle animated-effect-1">
                  <li class="facebook"><a target="_blank" href="https://www.facebook.com/colocdelafilaterie/"><i class="fa fa-facebook"></i></a></li>
                  <li class="pinterest"><a target="_blank" href="https://www.pinterest.fr/lescolocsdelafilaterie/"><i class="fa fa-pinterest"></i></a></li>
                </ul>
                <ul class="list-icons">
                  <li><i class="fa fa-map-marker pr-10 text-default"></i>1 rue de la Filaterie 74150 RUMILLY</li>
                  <li><i class="fa fa-phone pr-10 text-default"></i>06 26 98 53 76</li>
                </ul>
              </div>
              <div class="col-lg-0">
                  <div id="map-canvas">
                  </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="footer-content">
            <h2 class="title">Nous contacter</h2>
              <br>
            <?php include("formulaire.php");  ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- .footer end -->