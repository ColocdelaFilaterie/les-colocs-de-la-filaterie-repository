<!DOCTYPE html>
<!--[if IE 9]> <html lang="zxx" class="ie9"> <![endif]-->
<!--[if gt IE 9]> <html lang="zxx" class="ie"> <![endif]-->
<!--[if !IE]><!-->
<html dir="ltr" lang="zxx">
	<!--<![endif]-->

  <head>
    <meta charset="utf-8">
    <title>Les Colocs de la Filaterie| Questions</title>
    <meta name="description" content="The Project a Bootstrap-based, Responsive HTML5 Template">
    <meta name="author" content="htmlcoder.me">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.ico">

    <!-- Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Font Awesome CSS -->
    <link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Fontello CSS -->
    <link href="fonts/fontello/css/fontello.css" rel="stylesheet">

    <!-- Plugins -->
    <link href="plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="css/animations.css" rel="stylesheet">
    <link href="plugins/owlcarousel2/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="plugins/owlcarousel2/assets/owl.theme.default.min.css" rel="stylesheet">
    <link href="plugins/hover/hover-min.css" rel="stylesheet">		
    
    <!-- The Project's core CSS file -->
    <!-- Use css/rtl_style.css for RTL version -->
    <link href="css/style.css" rel="stylesheet" >
    <!-- The Project's Typography CSS file, includes used fonts -->
    <!-- Used font for body: Roboto -->
    <!-- Used font for headings: Raleway -->
    <!-- Use css/rtl_typography-default.css for RTL version -->
    <link href="css/typography-default.css" rel="stylesheet" >
    <!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
    <link href="css/skins/gold.css" rel="stylesheet">
    

    <!-- Custom css --> 
    <link href="css/custom.css" rel="stylesheet">
    
    
  </head>

  <!-- body classes:  -->
  <!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
  <!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
  <!-- "transparent-header": makes the header transparent and pulls the banner to top -->
  <!-- "gradient-background-header": applies gradient background to header -->
  <!-- "page-loader-1 ... page-loader-6": add a page loader to the page (more info @components-page-loaders.html) -->
  <body class=" ">

    <!-- scrollToTop -->
    <!-- ================ -->
    <div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>

    <!-- page wrapper start -->
    <!-- ================ -->
    <div class="page-wrapper">

        <?php include("menu.php");  ?>

      </div>
      <!-- header-container end -->
      <!-- main-container start -->
      <!-- ================ -->
      <section class="main-container">

        <div class="container">
          <div class="row">

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-lg-8">

              <!-- page-title start -->
              <!-- ================ -->
              <h1 class="page-title">Questions</h1>
              <div class="separator-2"></div>
              <!-- page-title end -->
              <p>Vous trouverez sur cette page toutes les réponses à vos questions! Si vous souhaitez nous en poser une n'hésitez pas à nous envoyer un mail!</p>
              <!-- Tab panes -->
              <div class="tab-content">
                <div class="tab-pane fade show active" id="tab1">
                  <!-- accordion start -->
                  <div id="accordion-faq" class="collapse-style-1" role="tablist" aria-multiselectable="true">
                    <div class="card">
                      <div class="card-header" role="tab" id="headingOne">
                        <h4 class="mb-0">
                          <a data-toggle="collapse" data-parent="#accordion-faq" href="#collapseOne" class="collapsed" aria-expanded="true" aria-controls="collapseOne">
                            <i class="fa fa-question-circle pr-10"></i>Les charges sont-elles comprises ?
                          </a>
                        </h4>
                      </div>
                      <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="card-block">
                          Les charges de copropriété correspondant notamment à l'entretient des parties communes sont comprises dans le montant du loyer. Les consommations d'éléctricité, eau, internet ne sont pas comprises dans le montant du loyer. Compter entre 40 et 50 € par mois, par locataire.
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header" role="tab" id="headingNine">
                        <h4 class="mb-0">
                          <a data-toggle="collapse" data-parent="#accordion-faq" href="#collapseNine" class="collapsed" aria-expanded="true" aria-controls="collapseNine">
                            <i class="fa fa-question-circle pr-10"></i>Quelle est la durée du préavis ?
                          </a>
                        </h4>
                      </div>
                      <div id="collapseNine" class="collapse" role="tabpanel" aria-labelledby="headingNine">
                        <div class="card-block">
                          La durée du préavis de sortie est de 1 mois. Il correspond au délai légal du bail meublé.
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header" role="tab" id="headingThree">
                        <h4 class="mb-0">
                          <a data-toggle="collapse" data-parent="#accordion-faq" href="#collapseThree" class="collapsed" aria-expanded="true" aria-controls="collapseThree">
                            <i class="fa fa-question-circle pr-10"></i>Quel est le montant de la caution ?
                          </a>
                        </h4>
                      </div>
                      <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="card-block">
                          Le montant de la caution est de 2X le montant d'un loyer.
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header" role="tab" id="headingTwo">
                        <h4 class="mb-0">
                          <a data-toggle="collapse" data-parent="#accordion-faq" href="#collapseTwo" class="collapsed" aria-expanded="true" aria-controls="collapseTwo">
                            <i class="fa fa-question-circle pr-10"></i>Quel est le délai de restitution de la caution ?
                          </a>
                        </h4>
                      </div>
                      <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="card-block">
                          Le délai légal de restitution de la caution est de 2 mois. Dans la plupart des cas, si aucune dégradation n'a été constatée lors de l'état des lieux de sortie, la caution est restituée dans un délai inférieur à 1 mois.
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header" role="tab" id="headingEight">
                        <h4 class="mb-0">
                          <a data-toggle="collapse" data-parent="#accordion-faq" href="#collapseEight" class="collapsed" aria-expanded="true" aria-controls="collapseHeight">
                            <i class="fa fa-question-circle pr-10"></i>Faut-il un garant ?
                          </a>
                        </h4>
                      </div>
                      <div id="collapseEight" class="collapse" role="tabpanel" aria-labelledby="headingHeight">
                        <div class="card-block">
                          Selon votre situation, un garant est un plus mais pas obligatoire.
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header" role="tab" id="headingSeven">
                        <h4 class="mb-0">
                          <a data-toggle="collapse" data-parent="#accordion-faq" href="#collapseSeven" class="collapsed" aria-expanded="true" aria-controls="collapseSeven">
                            <i class="fa fa-question-circle pr-10"></i>Il y a-t-il une cave ?
                          </a>
                        </h4>
                      </div>
                      <div id="collapseSeven" class="collapse" role="tabpanel" aria-labelledby="headingSeven">
                        <div class="card-block">
                          Chaque appartement est doté d'une cave individuelle au sous-sol.
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header" role="tab" id="headingSix">
                        <h4 class="mb-0">
                          <a data-toggle="collapse" data-parent="#accordion-faq" href="#collapseSix" class="collapsed" aria-expanded="true" aria-controls="collapseSix">
                            <i class="fa fa-question-circle pr-10"></i>Est-il facile de se garer ?
                          </a>
                        </h4>
                      </div>
                      <div id="collapseSix" class="collapse" role="tabpanel" aria-labelledby="headingSix">
                        <div class="card-block">
                          Oui, il existe 3 possibilités:
                          <ul>
                            <li>Au pied de l'immeuble, avec notamment des places "bleues". Gratuites de 19h à 8h du matin.</li>
                            <li>Dans les parkings du centre ville, situés à 2 minutes à pied.</li>
                            <li>Un grand parking public, situé à 5 minutes de l'immeuble, est disponible pour un stationement longue durée.</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- accordion end -->
                
                </div>
              


              </div>
            </div>
            <!-- main end -->

            <!-- sidebar start -->
            <!-- ================ -->
            <aside class="col-lg-4 col-xl-3 ml-xl-auto">
              <div class="sidebar">
                <div class="block clearfix">
                  <h3 class="title">Poser une question</h3>
                  <div class="separator-2"></div>
                   <?php include("formulaire.php");  ?>
                </div>
              </div>
            </aside>
            <!-- sidebar end -->

          </div>
        </div>
      </section>
      <!-- main-container end -->

      <!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
      <!-- ================ -->
      <footer id="footer" class="clearfix ">

                <?php include("footer.php");  ?>


      </footer>
      <!-- footer end -->
    </div>
    <!-- page-wrapper end -->

    <!-- JavaScript files placed at the end of the document so the pages load faster -->
    <!-- ================================================== -->
    <!-- Jquery and Bootstap core js files -->
    <script type="text/javascript" src="plugins/jquery.min.js"></script>
    <script type="text/javascript" src="plugins/popper.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Modernizr javascript -->
    <script type="text/javascript" src="plugins/modernizr.js"></script>
    <!-- Magnific Popup javascript -->
    <script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Appear javascript -->
    <script type="text/javascript" src="plugins/waypoints/jquery.waypoints.min.js"></script>
    <script type="text/javascript" src="plugins/waypoints/sticky.min.js"></script>
    <!-- Count To javascript -->
    <script type="text/javascript" src="plugins/jquery.countTo.js"></script>
    <!-- Parallax javascript -->
    <script src="plugins/jquery.parallax-1.1.3.js"></script>
    <!-- Contact form -->
    <script src="plugins/jquery.validate.js"></script>
    <!-- Owl carousel javascript -->
    <script type="text/javascript" src="plugins/owlcarousel2/owl.carousel.min.js"></script>
    <!-- Initialization of Plugins -->
    <script type="text/javascript" src="js/template.js"></script>
    <!-- Custom Scripts -->
    <script type="text/javascript" src="js/custom.js"></script>

  </body>
</html>
