<!DOCTYPE html>
<!--[if IE 9]> <html lang="zxx" class="ie9"> <![endif]-->
<!--[if gt IE 9]> <html lang="zxx" class="ie"> <![endif]-->
<!--[if !IE]><!-->
<html dir="ltr" lang="fran">
  <!--<![endif]-->

  <head>
    <meta charset="utf-8">
    <title>Studios</title>
    <meta name="description" content="Appartements en colocations la Flaterie, Rumilly 74 Haute Savoie">
    <meta name="author" content="htmlcoder.me">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.ico">

    <!-- Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Font Awesome CSS -->
    <link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Fontello CSS -->
    <link href="fonts/fontello/css/fontello.css" rel="stylesheet">

    <!-- Plugins -->
    <link href="plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="css/animations.css" rel="stylesheet">
    <link href="plugins/owlcarousel2/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="plugins/owlcarousel2/assets/owl.theme.default.min.css" rel="stylesheet">
    <link href="plugins/hover/hover-min.css" rel="stylesheet">    
    
    <!-- The Project's core CSS file -->
    <!-- Use css/rtl_style.css for RTL version -->
    <link href="css/style.css" rel="stylesheet" >
    <!-- The Project's Typography CSS file, includes used fonts -->
    <!-- Used font for body: Roboto -->
    <!-- Used font for headings: Raleway -->
    <!-- Use css/rtl_typography-default.css for RTL version -->
    <link href="css/typography-default.css" rel="stylesheet" >
    <!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
    <link href="css/skins/gold.css" rel="stylesheet">
    

    <!-- Custom css --> 
    <link href="css/custom.css" rel="stylesheet">
    
    
  </head>

  <!-- body classes:  -->
  <!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
  <!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
  <!-- "transparent-header": makes the header transparent and pulls the banner to top -->
  <!-- "gradient-background-header": applies gradient background to header -->
  <!-- "page-loader-1 ... page-loader-6": add a page loader to the page (more info @components-page-loaders.html) -->
  <body class=" ">

    <!-- scrollToTop -->
    <!-- ================ -->
    <div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>

    <!-- page wrapper start -->
    <!-- ================ -->
    <div class="page-wrapper">

      <?php include("menu.php");  ?>
      <!-- banner start -->
      <!-- ================ -->
      <div class="banner border-clear light-translucent-bg" style="background-image:url('images/studios0.jpg');background-position: 50% 32%;">
        <div class="container">
          <div class="row justify-content-lg-center">
            <div class="col-lg-8 text-center pv-20">
              <h2 class="title" data-animation-effect="fadeIn" data-effect-delay="100"> <strong>Les Studios</strong></h2>
              <div class="separator mt-10" data-animation-effect="fadeIn" data-effect-delay="100"></div>
              <p class="text-center" data-animation-effect="fadeIn" data-effect-delay="100">Appartements à louer pour une personne.</p>
            </div>
          </div>
        </div>
      </div>
      <!-- banner end -->

      <!-- main-container start -->
      <!-- ================ -->
      <section class="main-container">

        <div class="container">
          <div class="row">

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-12">
              <!-- pills start -->
              <!-- ================ -->
              <!-- Tab panes -->
              <div class="tab-content clear-style">
                <div class="tab-pane active" id="pill-1">           
                  <div class="row masonry-grid-fitrows grid-space-10">
                    <div class="col-lg-5 masonry-grid-item">
                      <div class="listing-item white-bg bordered mb-20">
                        <div class="overlay-container">
                          <img src="images/studio-0.jpg" alt="Le Studio">
                          <a class="overlay-link" href="le-studio.php"><i class="fa fa-plus"></i></a>
                          <span class="badge">Disponible Août 2019</span>
                        </div>
                        <div class="body">
                          <h3><a href="mille-cabanes.php">Le Studio</a></h3>
                          <p class="small">Petit et très cosy, vous serz ravis par ce studio moderne.</p>
                          <div class="elements-list clearfix">
                            <span class="price">450 €/mois</span>
                            <a href="le-studio.php" class="pull-right margin-clear btn btn-sm btn-default-transparent ">Détails</a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-5 masonry-grid-item">
                      <div class="listing-item white-bg bordered mb-20">
                        <div class="overlay-container">
                          <img src="images/fenetres-0.jpg" alt="Mille Fenêtres">
                          <a class="overlay-link" href="mille-fenetres.php"><i class="fa fa-plus"></i></a>
                          <span class="badge">Dispoible Août 2019</span>
                        </div>
                        <div class="body">
                          <h3><a href="mille-fenetres.php">Rose Bonbon</a></h3>
                          <p class="small">Avec sa verrière, vous découvrirez un appartement très ouvert.</p>
                          <div class="elements-list clearfix">
                            <span class="price">540 €/mois</span>
                            <a href="mille-fenetres.php" class="pull-right margin-clear btn btn-sm btn-default-transparent ">Détails</a>
                          </div>
                        </div>
                      </div>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- pills end -->
            </div>
            <!-- main end -->

          </div>
        </div>
      </section>
      <!-- main-container end -->

      
      <!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
      <!-- ================ -->
      <footer id="footer" class="clearfix ">


                <?php include("footer.php");  ?>


      </footer>
      <!-- footer end -->
    </div>
    <!-- page-wrapper end -->

    <!-- JavaScript files placed at the end of the document so the pages load faster -->
    <!-- ================================================== -->
    <!-- Jquery and Bootstap core js files -->
    <script type="text/javascript" src="plugins/jquery.min.js"></script>
    <script type="text/javascript" src="plugins/popper.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Modernizr javascript -->
    <script type="text/javascript" src="plugins/modernizr.js"></script>
    <!-- Isotope javascript -->
    <script type="text/javascript" src="plugins/isotope/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="plugins/isotope/isotope.pkgd.min.js"></script>
    <!-- Magnific Popup javascript -->
    <script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Appear javascript -->
    <script type="text/javascript" src="plugins/waypoints/jquery.waypoints.min.js"></script>
    <script type="text/javascript" src="plugins/waypoints/sticky.min.js"></script>
    <!-- Count To javascript -->
    <script type="text/javascript" src="plugins/jquery.countTo.js"></script>
    <!-- Parallax javascript -->
    <script src="plugins/jquery.parallax-1.1.3.js"></script>
    <!-- Contact form -->
    <script src="plugins/jquery.validate.js"></script>
    <!-- Owl carousel javascript -->
    <script type="text/javascript" src="plugins/owlcarousel2/owl.carousel.min.js"></script>
    <!-- Initialization of Plugins -->
    <script type="text/javascript" src="js/template.js"></script>
    <!-- Custom Scripts -->
    <script type="text/javascript" src="js/custom.js"></script>

  </body>
</html>
