(function($){
	$(document).ready(function(){

		// Google Maps
		//-----------------------------------------------
		if ($("#map-canvas").length>0) {
			var map, myLatlng, myZoom, marker;
			// Set the coordinates of your location
			myLatlng = new google.maps.LatLng(<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d9015.08530794716!2d5.941066908629088!3d45.867711382191764!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1s1+rue+filaterie+!5e0!3m2!1sfr!2sfr!4v1556267865568!5m2!1sfr!2sfr" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>);
			myZoom = 0;
			function initialize() {
				var mapOptions = {
					zoom: myZoom,
					mapTypeId: google.maps.MapTypeId.ROADMAP,
					center: myLatlng,
					scrollwheel: false
				};
				map = new google.maps.Map(document.getElementById("map-canvas"),mapOptions);
				marker = new google.maps.Marker({
					map:map,
					draggable:true,
					animation: google.maps.Animation.DROP,
					position: myLatlng
				});
				google.maps.event.addDomListener(window, "resize", function() {
					map.setCenter(myLatlng);
				});

				$('#collapseMap').on('shown.bs.collapse', function() {
					google.maps.event.trigger(map, 'resize');
					map.setCenter(myLatlng);
				});
			}
			google.maps.event.addDomListener(window, "load", initialize);
		}
	}); // End document ready

})(this.jQuery);		